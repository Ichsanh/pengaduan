<style>
    li {
        text-align: justify;
    }
</style>
<section id="home" class="main-banner parallaxie" style="background: url('<?= base_url(); ?>/aset/images/bps_aceh.jpg')">
    <div class="heading">
        <h1>Sistem Pelaporan Gratifikasi</h1>
        <h3 class="cd-headline clip is-full-width">
            <span>Laporkan Jika Anda Menerima Gratifikasi: </span>
            <span class="cd-words-wrapper">
                <b class="is-visible">Barang</b>
                <b>Uang</b>
                <b>Lainnya</b>
                <b>di BPS Provinsi Aceh</b>
            </span>
            <div class="btn-ber">

                <?php if (!$this->session->userdata('logged')) {
                ?>

                    <a class="learn_btn hvr-bounce-to-top" href="#" data-toggle="modal" data-target="#modal_login">Login</a>
                <?php
                } else {
                ?>
                    <a class="get_btn hvr-bounce-to-top" href="#laporkan">Laporkan</a>
                <?php
                } ?>

            </div>
        </h3>
    </div>
</section>

<div id="kategori" class="section wb">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="message-box">

                    <h2>Kategori Gratifikasi</h2>
                    Contoh-contoh Pemberian yang dapat dikategorikan sebagai Gratifikasi :
                    <ul>
                        <li>1. Pemberian hadiah atau uang sebagai ucapan terima kasih karena telah dibantu
                            Hadiah atau sumbangan pada saat perkawinan anak dari pejabat oleh rekanan kantor pejabat
                            tersebut</li>
                        <li>2. Pemberian tiket perjalanan kepada pejabat atau keluarganya untuk keperluan pribadi
                            secara
                            cuma-cuma</li>
                        <li>3. Pemberian potongan harga khusus bagi pejabat untuk pembelian barang atau jasa dari
                            rekanan</li>
                        <li>4. Pemberian biaya atau ongkos naik haji dari rekanan kepada pejabat</li>
                        <li>5. Pemberian hadiah ulang tahun atau pada acara-acara pribadi lainnya dari rekanan
                        </li>
                        <li>6. Pemberian hadiah atau souvenir kepada pejabat pada saat kunjungan kerja
                        </li>
                        <li>7. Pemberian hadiah atau parsel kepada pejabat pada saat hari raya keagamaan, oleh
                            rekanan
                            atau bawahannya.
                        </li>
                        <li>8. Seluruh pemberian tersebut diatas, dapat dikategorikan sebagai gratifikasi, apalbila
                            ada
                            hubungan kerja atau kedinasan antara pemberi dan dengan pejabat yang menerima, dan/atau
                            semata-mata karena keterkaitan dengan jabatan atau kedudukan pejabat tersebut.
                        </li>
                    </ul>

                    <!-- <a href="#" class="sim-btn hvr-bounce-to-top"><span>Contact Us</span></a> -->
                </div><!-- end messagebox -->
            </div><!-- end col -->

            <div class="col-md-6">
                <!-- <div class="our-team"> -->
                <!-- <div class="pic"> -->
                <div class="text-center">
                    <img src="<?= base_url(); ?>/aset/images/edo.png" style="border-radius: 20%; max-height: 600px" class="img-fluid">
                </div>
                <!-- </div> -->
                <!-- </div> -->
                <!-- <div class="right-box-pro wow fadeIn img-rounded">
                        <img src="<?= base_url(); ?>/aset/uploads/gallery_img-01.jpg" alt=""
                            class="img-fluid img-rounded">
                    </div> -->
                <!-- end media -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->


<div id="laporkan" class="section db">
    <div class="container">
        <div class="section-title text-center">
            <h3 style="color: white">Form Laporan</h3>
            <!-- <p>Quisque eget nisl id nulla sagittis auctor quis id. Aliquam quis vehicula enim, non aliquam risus.</p> -->
        </div><!-- end title -->

        <div class="row">
            <div class="col-md-12">
                <div class="contact_form">
                    <div id="message"></div>
                    <!-- <form id="contactForm" name="sentMessage" novalidate="novalidate" method="post" enctype="multipart/form-data" action="/input"> -->
                    <form method="post" enctype="multipart/form-data" action="<?= base_url(); ?>home/input">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" id="nama" type="text" name="nama" placeholder="Nama Pelapor" required="required" data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="nip" type="text" name="nip" placeholder="NIP Pelapor" required="required" data-validation-required-message="Please enter your nip.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="nama_terlapor" type="text" name="nama_terlapor" placeholder="Nama Terlapor" required="required" data-validation-required-message="Silakan isikan nama terlapor.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="alamat_terlapor" type="text" name="alamat_terlapor" placeholder="Alamat Terlapor" required="required" data-validation-required-message="Silakan isikan Alamat terlapor.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" required="required" style="height: 40px;" name="pekerjaan_terlapor">
                                        <option>Pekerjaan Terlapor</option>
                                        <option value="pns">PNS</option>
                                        <option value="non pns">Non PNS</option>
                                    </select>
                                    <p class="help-block text-danger"></p>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <select class="form-control" required="required" style="height: 40px;" name="jenis_gratifikasi">
                                        <option value="">Jenis Gratifikasi</option>
                                        <?php foreach ($jenis_gratifikasi as $key => $value) {
                                        ?>

                                            <option value="<?= $value['id_jenis_gratifikasi']; ?>"><?= $value['ket_gratifikasi'] ?></option>

                                        <?php
                                        } ?>
                                    </select>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="tempat" type="text" name="tempat" placeholder="Tempat Kejadian" required="required" data-validation-required-message="Silakan Isikan Tempat Kejadian.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" onmouseover="(this.type='date')" id="waktu" name="waktu" placeholder="Waktu Kejadian" required="required" data-validation-required-message="Silakan Isikan Waktu Kejadian.">
                                </div>

                                <div class="form-group">
                                    <select class="form-control" required="required" style="height: 40px;" name="jenis_layanan">
                                        <option value="">Jenis Layanan</option>
                                        <?php foreach ($jenis_layanan as $key => $value) {
                                        ?>

                                            <option value="<?= $value['id_jenis_layanan']; ?>"><?= $value['ket_layanan'] ?></option>

                                        <?php
                                        } ?>
                                    </select>
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="nilai" type="number" name="nilai" placeholder="Nilai Gratifikasi (Rupiah)" required="required" data-validation-required-message="Silakan Isikan Nilai Gratifikasi.">
                                    <p class="help-block text-danger"></p>
                                </div>


                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <!-- <button id="sendMessageButton" class="sim-btn hvr-bounce-to-top" type="submit">Send
                                    Message</button> -->
                                <button type="submit" class="sim-btn hvr-bounce-to-top btn-sm">kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->

<div id="list" class="section wb">
    <div class="container">
        <div class="section-title text-center">
            <h3>List Laporan</h3>
            <!-- <p>Quisque eget nisl id nulla sagittis auctor quis id. Aliquam quis vehicula enim, non aliquam risus.</p> -->
        </div><!-- end title -->

        <!-- <div class="row"> -->
        <div class="section-title ">
            <table id="table_id" style="width: 100%;">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Pelapor</th>
                        <th>Nama Terlapor</th>
                        <th>Jenis Gratifikasi</th>
                        <th>Nilai</th>
                        <th>Aksi</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($data)) {
                        # code...

                        $i = 1;
                        foreach ($data as $key => $item) {
                    ?>

                            <tr>
                                <td><?= $i++; ?> </td>
                                <td><?= $item['nama']; ?> </td>
                                <td><?= $item['nama_terlapor']; ?> </td>
                                <td><?= $item['ket_gratifikasi']; ?> </td>
                                <td><?= $item['nilai']; ?> </td>
                                <td><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_detail" onclick="showuserdetail(<?= $item['id_laporan']; ?>)">Detail</button></td>
                            </tr>
                    <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <!-- </div> -->
        <!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->


<!-- Modal -->
<div class="modal fade" id="modal_login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form method="post" enctype="multipart/form-data" action="<?= base_url(); ?>home/login">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Form Login</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <input class="form-control" id="username" type="text" name="username" placeholder="Username" required="required" data-validation-required-message="Silakan Isikan Username.">
                        <p class="help-block text-danger"></p>
                    </div>
                    <div class="form-group">
                        <input class="form-control" id="password" type="text" name="password" placeholder="Password" required="required" data-validation-required-message="Silakan Isikan Password.">
                        <p class="help-block text-danger"></p>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLongTitle">Detail Laporan</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div id="bodymodal_userDetail"></div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <!-- <button type="button" class="btn btn-primary">Login</button> -->
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    function showuserdetail(id) {
        $.ajax({
            type: "post",
            url: "<?= site_url('ajax/get_laporan'); ?>",
            data: "id=" + id,
            dataType: "html",
            success: function(response) {
                $('#bodymodal_userDetail').empty();
                $('#bodymodal_userDetail').append(response);
            }
        });
    }
</script>