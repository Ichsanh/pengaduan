<!DOCTYPE html>
<html lang="en">

<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Site Metas -->
<title>Sistem Pelaporan Gratifikasi BPS Provinsi Aceh</title>
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="author" content="">

<!-- Site Icons -->
<link rel="shortcut icon" href="<?= base_url(); ?>/aset/images/logo_bps.png" type="image/x-icon" />
<link rel="apple-touch-icon" href="<?= base_url(); ?>/aset/images/logo_bps.png">

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" -->
<!-- integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?= base_url(); ?>/aset/css/bootstrap.min.css">
<!-- Site CSS -->
<link rel="stylesheet" href="<?= base_url(); ?>/aset/style.css">
<!-- Responsive CSS -->
<link rel="stylesheet" href="<?= base_url(); ?>/aset/css/responsive.css">
<!-- Custom CSS -->
<link rel="stylesheet" href="<?= base_url(); ?>/aset/css/custom.css">
<script src="<?= base_url(); ?>/aset/js/modernizr.js"></script> <!-- Modernizr -->
<!-- datatable CSS -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">




<!--[if lt IE 9]>
      <script src="<?= base_url(); ?>/aset/https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="<?= base_url(); ?>/aset/https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="politics_version">

    <!-- LOADER -->
    <div id="preloader">
        <div id="main-ld">
            <div id="loader"></div>
        </div>
    </div><!-- end loader -->
    <!-- END LOADER -->

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top">
                <img class="img-fluid" src="<?= base_url(); ?>/aset/images/logo_bps_header.png" alt="" />
            </a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ml-auto">
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger active" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#kategori">Kategori</a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#blog">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#team">Our Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="#pricing">Pricing</a>
                    </li> -->

                    <?php
                    if ($this->session->userdata('logged')) {
                        if ($this->session->userdata('level') <> 1) {
                    ?>
                            <li class="nav-item">
                                <a class="nav-link js-scroll-trigger" href="#laporkan">Laporkan</a>
                            </li>
                        <?php
                        }
                        ?>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#list">List</a>
                        </li>
                    <?php
                    }
                    ?>


                    <?php
                    if ($this->session->userdata('logged')) {
                    ?>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="<?= base_url(); ?>home/logout">Logout</a>
                        </li>
                    <?php
                    } else {
                    ?>
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#" data-toggle="modal" data-target="#modal_login">Login</a>
                        </li>

                    <?php } ?>


                </ul>
            </div>
        </div>
    </nav>
    <!-- awal content -->
    <?php
    echo $contents;
    ?>
    <!-- akhir konten -->


    <div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
                <!-- <a href="#"><img src="<?= base_url(); ?>/aset/images/logo.png" alt="" /></a> -->
                <div class="footer-center">
                    <p class="footer-links">

                        <a href="#home">Home</a>
                        <a href="#kategori">Kategori</a>

                        <?php
                        if ($this->session->userdata('logged')) {
                            echo '<a href="#laporkan">Laporkan</a>';
                            if ($this->session->userdata('level') <> 1) {
                            }
                        ?>
                            <a href="#list">List</a>
                        <?php
                        }
                        ?>
                        <?php
                        if ($this->session->userdata('logged')) {
                        ?>
                            <a href="<?= base_url(); ?>home/logout">Logout</a>
                        <?php
                        } else {
                        ?>
                            <a href="#" data-toggle="modal" data-target="#modal_login">Login</a>
                        <?php } ?>

                    </p>
                    <p class="footer-company-name">Badan Pusat Statistik Provinsi Aceh
                        <!-- <a href="https://html.design/">html design</a> -->
                    </p>
                </div>
            </div>
        </div><!-- end container -->
    </div><!-- end copyrights -->

    <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="<?= base_url(); ?>/aset/js/all.js"></script>
    <!-- Camera Slider -->
    <!-- <script src="<?= base_url(); ?>/aset/js/jquery.mobile.customized.min.js"></script> -->
    <script src="<?= base_url(); ?>/aset/js/jquery.easing.1.3.js"></script>
    <script src="<?= base_url(); ?>/aset/js/parallaxie.js"></script>
    <script src="<?= base_url(); ?>/aset/js/headline.js"></script>
    <!-- Contact form JavaScript -->
    <script src="<?= base_url(); ?>/aset/js/jqBootstrapValidation.js"></script>
    <script src="<?= base_url(); ?>/aset/js/contact_me.js"></script>
    <!-- ALL PLUGINS -->
    <script src="<?= base_url(); ?>/aset/js/custom.js"></script>
    <script src="<?= base_url(); ?>/aset/js/jquery.vide.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.js"></script>

    <!-- 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script> -->
    <script>
        $(document).ready(function() {
            $('#table_id').DataTable({
                "scrollX": true
            });
            var level = <?= ($this->session->userdata('logged')) ? $this->session->userdata('level') : 0;  ?>;
            if (level) {
                if (level != 1) {
                    $('#list').remove();
                }
            } else {
                $('#laporkan').remove();
                $('#list').remove();
            }

        });
    </script>
</body>

</html>