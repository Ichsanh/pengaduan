<?php

class Laporan_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        return $this->db->select('*')->from('sigra_laporan')
            ->join('sigra_jenis_gratifikasi', 'sigra_laporan.jenis_gratifikasi = sigra_jenis_gratifikasi.id_jenis_gratifikasi')
            ->join('sigra_jenis_layanan', 'sigra_laporan.jenis_layanan = sigra_jenis_layanan.id_jenis_layanan')
            ->get()->result_array();
    }
    function get_id($id)
    {
        return $this->db->select('*')->from('sigra_laporan')
            ->join('sigra_jenis_gratifikasi', 'sigra_laporan.jenis_gratifikasi = sigra_jenis_gratifikasi.id_jenis_gratifikasi')
            ->join('sigra_jenis_layanan', 'sigra_laporan.jenis_layanan = sigra_jenis_layanan.id_jenis_layanan')
            ->where('id_laporan', $id)
            ->get()->row_array();
    }
} //akhir class