<?php

class Login_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function cek($username, $password)
    {
        return $this->db->select('*')->from('sigra_master_pegawai')->where('nip', $username)->where('password', $password)->get()->row_array();
    }
    function cek_upg($nip)
    {
        return $this->db->select('*')->from('sigra_upg')->where('nip', $nip)->get()->row_array();
    }
} //akhir class