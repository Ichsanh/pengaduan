<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajax extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('master_model');
        $this->load->model('laporan_model');
    }

    public function get_laporan()
    {
        $laporan = $this->laporan_model->get_id($this->input->post('id'));
?>
        <table class=”table table-bordered”>
            <tbody>
                <tr>
                    <th>Nama</th>
                    <th>:</th>
                    <th><?= $laporan['nama'] ?></th>
                </tr>
                <tr>
                    <th>NIP</th>
                    <th>:</th>
                    <th><?= $laporan['nip'] ?></th>
                </tr>
                <tr>
                    <th>Nama Terlapor</th>
                    <th>:</th>
                    <th><?= $laporan['nama_terlapor'] ?></th>
                </tr>
                <tr>
                    <th>Alamat Terlapor</th>
                    <th>:</th>
                    <th><?= $laporan['alamat_terlapor'] ?></th>
                </tr>
                <tr>
                    <th>Pekerjaan Terlapor</th>
                    <th>:</th>
                    <th><?= $laporan['pekerjaan_terlapor'] ?></th>
                </tr>
                <tr>
                    <th>Jenis Gratifikasi</th>
                    <th>:</th>
                    <th><?= $laporan['ket_gratifikasi'] ?></th>
                </tr>
                <tr>
                    <th>Tempat</th>
                    <th>:</th>
                    <th><?= $laporan['tempat'] ?></th>
                </tr>
                <tr>
                    <th>Waktu</th>
                    <th>:</th>
                    <th><?= date('d-m-Y', strtotime($laporan['waktu'])) ?></th>
                </tr>
                <tr>
                    <th>Jenis Layanan</th>
                    <th>:</th>
                    <th><?= $laporan['ket_layanan'] ?></th>
                </tr>
                <tr>
                    <th>Nilai (Rp)</th>
                    <th>:</th>
                    <th><?= $laporan['nilai'] ?></th>
                </tr>

            </tbody>
        </table>
<?php
    }
}
