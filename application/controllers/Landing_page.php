<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing_page extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // $this->load->model('mahasiswa_model');
    }

    public function index()
    {

        $this->load->view('landing_page/landing_page');
    }
}
