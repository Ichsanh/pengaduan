<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('master_model');
        $this->load->model('login_model');
        $this->load->model('laporan_model');
    }

    public function index()
    {
        $data['halaman'] = 'cari_pustaka';
        if ($this->session->userdata('level') == 1) {
            $data['data'] = $this->laporan_model->get_all();
        } else {
            $data['data'] = null;
        }
        $data['jenis_gratifikasi'] = $this->master_model->get_all('sigra_jenis_gratifikasi');
        $data['jenis_layanan'] = $this->master_model->get_all('sigra_jenis_layanan');

        $this->load->vars($data);
        $this->template->load('template/template_paralax', 'home');
    }

    public function input()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('nip', 'nip', 'required');
        if ($this->form_validation->run()) // ini filter
        {
            $params = array(
                'nama' => $this->input->post('nama'),
                'nip' => $this->input->post('nip'),
                'waktu' => ($this->input->post('waktu')) ? date("Y-m-d", strtotime($this->input->post('waktu'))) : '',
                'nama_terlapor' => $this->input->post('nama_terlapor'),
                'alamat_terlapor' => $this->input->post('alamat_terlapor'),
                'pekerjaan_terlapor' => $this->input->post('pekerjaan_terlapor'),
                'jenis_gratifikasi' => $this->input->post('jenis_gratifikasi'),
                'jenis_layanan' => $this->input->post('jenis_layanan'),
                'tempat' => $this->input->post('tempat'),
                'nilai' => $this->input->post('nilai'),
            );
            $this->master_model->insert('sigra_laporan', $params);
            redirect('home');
        } else {
            redirect('home');
        }
    }

    function logout() // fungsi logout
    {
        $this->session->sess_destroy();
        redirect('home');
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $cek = $this->login_model->cek($username, $password); // cek kebenaran username dan password

        if (!empty($cek)) {

            $sess_data['username'] = $cek['nip'];
            $sess_data['nama'] = $cek['nama'];
            $sess_data['logged'] = TRUE;
            $sess_data['level'] = 0;
            $this->session->set_userdata($sess_data);

            $upg = $this->login_model->cek_upg($cek['nip']);
            if (!empty($upg)) {
                $sess_data['level'] = 1;
                $this->session->set_userdata($sess_data);
            }

            redirect('home');

            // $this->session->set_flashdata('pesan', 'Maaf, kombinasi username dengan password benar.');
        } else {
            // jika username dan password tidak sesuai
            $this->session->set_flashdata('pesan', 'Maaf, kombinasi username dengan password salah.');
            redirect('home');
        }
    }
}
