-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 23, 2020 at 09:55 AM
-- Server version: 10.3.24-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bps1100_db_hendra`
--

-- --------------------------------------------------------

--
-- Table structure for table `sigra_jenis_gratifikasi`
--

CREATE TABLE `sigra_jenis_gratifikasi` (
  `id_jenis_gratifikasi` int(11) NOT NULL,
  `ket_gratifikasi` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sigra_jenis_gratifikasi`
--

INSERT INTO `sigra_jenis_gratifikasi` (`id_jenis_gratifikasi`, `ket_gratifikasi`) VALUES
(1, 'Barang'),
(2, 'Uang'),
(3, 'Lainnya');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sigra_jenis_gratifikasi`
--
ALTER TABLE `sigra_jenis_gratifikasi`
  ADD PRIMARY KEY (`id_jenis_gratifikasi`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sigra_jenis_gratifikasi`
--
ALTER TABLE `sigra_jenis_gratifikasi`
  MODIFY `id_jenis_gratifikasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
