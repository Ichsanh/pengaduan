-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 23, 2020 at 09:55 AM
-- Server version: 10.3.24-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bps1100_db_hendra`
--

-- --------------------------------------------------------

--
-- Table structure for table `sigra_master_pegawai`
--

CREATE TABLE `sigra_master_pegawai` (
  `id` int(11) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sigra_master_pegawai`
--

INSERT INTO `sigra_master_pegawai` (`id`, `nip`, `nama`, `password`) VALUES
(1, '340012998', 'Abd. Hakim, S.Si, M.Si', '340012998'),
(2, '340019980', 'Afrizah', '340019980'),
(3, '340019955', 'Akhmad Sugito, SST, M.Si', '340019955'),
(4, '340012902', 'Amiruddin', '340012902'),
(5, '340057304', 'Ar Razy Ridha Maulana, SST', '340057304'),
(6, '340016937', 'Armelia Amri, SST, M.Si.', '340016937'),
(7, '340016304', 'Azwar, S.E', '340016304'),
(8, '340054422', 'Bahzur Andria, SE', '340054422'),
(9, '340017511', 'Chairina', '340017511'),
(10, '340013777', 'Chalida Rahmi, SE, M.M.', '340013777'),
(11, '340013776', 'Cut Emalita, SE.', '340013776'),
(12, '340013412', 'Cut Malahayati, SE', '340013412'),
(13, '340016704', 'Cut Sandra Novalia', '340016704'),
(14, '340015925', 'Dadan Supriadi, SST, M.Si', '340015925'),
(15, '340017289', 'Devi Indriastuti, SST, M.Si.', '340017289'),
(16, '340054424', 'Dewi Putri Rahayu, A.Md.', '340054424'),
(17, '132184671', 'Dr Azwar, S.Pd.,M.Si', '132184671'),
(18, '340018220', 'Dwi Susanti', '340018220'),
(19, '340017510', 'Edo Mara Nasution, S.E.', '340017510'),
(20, '340015334', 'Effendy, S.Si', '340015334'),
(21, '340051106', 'Eko Dwi Mulyono, S.Si', '340051106'),
(22, '340016705', 'Fadlun Andrian, SE, MM', '340016705'),
(23, '340019971', 'Ferfi Hamdina, SE.', '340019971'),
(24, '340019964', 'Fitral Pratomo, SST', '340019964'),
(25, '340017783', 'Haifa Sari, SST, M.Si.', '340017783'),
(26, '340053004', 'Hanafiah', '340053004'),
(27, '340017788', 'Hani`ah, SST., M.Ec.Dev', '340017788'),
(28, '340050109', 'Hendra Dharmawan, SST', '340050109'),
(29, '340017787', 'Hendra Gunawan, SST', '340017787'),
(30, '340050110', 'Hendri Achmad Hudori, SST, M.Si.', '340050110'),
(31, '340011682', 'Ihsanurijal, S.Si, M.Si', '340011682'),
(32, '340018971', 'Ikhsanuddin, SE', '340018971'),
(33, '340012874', 'Ir. Andariati Afrida', '340012874'),
(34, '340057682', 'Ismaturrahmi Suhaimi, SST', '340057682'),
(35, '340014050', 'Israwati, SE', '340014050'),
(36, '340054438', 'Iswahyudi, A.Md', '340054438'),
(37, '340016699', 'Ita Meriati, SE', '340016699'),
(38, '340054441', 'Juliana, SP', '340054441'),
(39, '340050135', 'Kahar Muzzakar, SST', '340050135'),
(40, '340012989', 'Kenda Paryatno, S.Si, M.E.', '340012989'),
(41, '340019966', 'M. Alimuddin, SST, M.T', '340019966'),
(42, '340019985', 'M. Reza, SE', '340019985'),
(43, '340018215', 'Maulidya', '340018215'),
(44, '340050153', 'Melly, SST', '340050153'),
(45, '340054448', 'Muhammad Adil, SH', '340054448'),
(46, '340017790', 'Muhammad Ridha, SST', '340017790'),
(47, '340019805', 'Mukhlis', '340019805'),
(48, '340053472', 'Munandar, S.Si', '340053472'),
(49, '340050167', 'Mursalina, SST', '340050167'),
(50, '340055974', 'Nasliyah Husna, S.P', '340055974'),
(51, '340054453', 'Nelvi Istiqamah, SE', '340054453'),
(52, '340050175', 'Nilam Humaira, SST', '340050175'),
(53, '340054454', 'NOVA MISRINA, A.Md', '340054454'),
(54, '340050186', 'Nur Hasanah, SST', '340050186'),
(55, '340055877', 'Nurhani Restu Umi, SST', '340055877'),
(56, '340017786', 'Nuri Rosmika, SST', '340017786'),
(57, '340012844', 'Nuriah Ismail, SE', '340012844'),
(58, '340011646', 'Nurmalawati, S.E.', '340011646'),
(59, '340013022', 'Nurzakiah, SE', '340013022'),
(60, '340018213', 'Nyimas Eka Yunia Hasnita, S.P', '340018213'),
(61, '340055879', 'Opan Fauzan Hamdan, SST, MEKK', '340055879'),
(62, '340015121', 'Oriza Santifa, S.Si, M.Si.', '340015121'),
(63, '340018969', 'Rais Fahmi, S.P', '340018969'),
(64, '340018216', 'Raiski Ramadhoni, S.E.', '340018216'),
(65, '340054458', 'Rediansyah Arief, SP, M.Si', '340054458'),
(66, '340019957', 'Reza Rezki Pulungan, SST', '340019957'),
(67, '340018566', 'Ridha Mutia, SE, M.Si', '340018566'),
(68, '340057201', 'Rizki Hadiman, SST', '340057201'),
(69, '340010990', 'Sakdani, SE.', '340010990'),
(70, '340015336', 'Tasdik Ilhamudin, S.Si, M.Si', '340015336'),
(71, '340051351', 'Teti Safrida Purba, SE.', '340051351'),
(72, '340016731', 'Titiek Zurriyati, S.Si', '340016731'),
(73, '340054471', 'Wahdini, S.Psi.', '340054471'),
(74, '340016943', 'Wahyu Agung Sutikno, SST', '340016943'),
(75, '340053247', 'Zelvia Yunalda, S.ST', '340053247'),
(76, '340054478', 'Zukhra, SE', '340054478'),
(77, '340014049', 'Zulkarnain', '340014049'),
(78, '340018218', 'Zulviyana', '340018218'),
(79, '340058260', 'FITRI MULYANI, SST', '340058260'),
(80, '340058287', 'ICHSAN HASANUDIN, SST', '340058287'),
(81, '340012245', 'RUBAMA', '340012245'),
(82, '340012998', 'Abd. Hakim, S.Si, M.Si', '340012998'),
(83, '340019980', 'Afrizah', '340019980'),
(84, '340019955', 'Akhmad Sugito, SST, M.Si', '340019955'),
(85, '340012902', 'Amiruddin', '340012902'),
(86, '340057304', 'Ar Razy Ridha Maulana, SST', '340057304'),
(87, '340016937', 'Armelia Amri, SST, M.Si.', '340016937'),
(88, '340016304', 'Azwar, S.E', '340016304'),
(89, '340054422', 'Bahzur Andria, SE', '340054422'),
(90, '340017511', 'Chairina', '340017511'),
(91, '340013777', 'Chalida Rahmi, SE, M.M.', '340013777'),
(92, '340013776', 'Cut Emalita, SE.', '340013776'),
(93, '340013412', 'Cut Malahayati, SE', '340013412'),
(94, '340016704', 'Cut Sandra Novalia', '340016704'),
(95, '340015925', 'Dadan Supriadi, SST, M.Si', '340015925'),
(96, '340017289', 'Devi Indriastuti, SST, M.Si.', '340017289'),
(97, '340054424', 'Dewi Putri Rahayu, A.Md.', '340054424'),
(98, '132184671', 'Dr Azwar, S.Pd.,M.Si', '132184671'),
(99, '340018220', 'Dwi Susanti', '340018220'),
(100, '340017510', 'Edo Mara Nasution, S.E.', '340017510'),
(101, '340015334', 'Effendy, S.Si', '340015334'),
(102, '340051106', 'Eko Dwi Mulyono, S.Si', '340051106'),
(103, '340016705', 'Fadlun Andrian, SE, MM', '340016705'),
(104, '340019971', 'Ferfi Hamdina, SE.', '340019971'),
(105, '340019964', 'Fitral Pratomo, SST', '340019964'),
(106, '340017783', 'Haifa Sari, SST, M.Si.', '340017783'),
(107, '340053004', 'Hanafiah', '340053004'),
(108, '340017788', 'Hani`ah, SST., M.Ec.Dev', '340017788'),
(109, '340050109', 'Hendra Dharmawan, SST', '340050109'),
(110, '340017787', 'Hendra Gunawan, SST', '340017787'),
(111, '340050110', 'Hendri Achmad Hudori, SST, M.Si.', '340050110'),
(112, '340011682', 'Ihsanurijal, S.Si, M.Si', '340011682'),
(113, '340018971', 'Ikhsanuddin, SE', '340018971'),
(114, '340012874', 'Ir. Andariati Afrida', '340012874'),
(115, '340057682', 'Ismaturrahmi Suhaimi, SST', '340057682'),
(116, '340014050', 'Israwati, SE', '340014050'),
(117, '340054438', 'Iswahyudi, A.Md', '340054438'),
(118, '340016699', 'Ita Meriati, SE', '340016699'),
(119, '340054441', 'Juliana, SP', '340054441'),
(120, '340050135', 'Kahar Muzzakar, SST', '340050135'),
(121, '340012989', 'Kenda Paryatno, S.Si, M.E.', '340012989'),
(122, '340019966', 'M. Alimuddin, SST, M.T', '340019966'),
(123, '340019985', 'M. Reza, SE', '340019985'),
(124, '340018215', 'Maulidya', '340018215'),
(125, '340050153', 'Melly, SST', '340050153'),
(126, '340054448', 'Muhammad Adil, SH', '340054448'),
(127, '340017790', 'Muhammad Ridha, SST', '340017790'),
(128, '340019805', 'Mukhlis', '340019805'),
(129, '340053472', 'Munandar, S.Si', '340053472'),
(130, '340050167', 'Mursalina, SST', '340050167'),
(131, '340055974', 'Nasliyah Husna, S.P', '340055974'),
(132, '340054453', 'Nelvi Istiqamah, SE', '340054453'),
(133, '340050175', 'Nilam Humaira, SST', '340050175'),
(134, '340054454', 'NOVA MISRINA, A.Md', '340054454'),
(135, '340050186', 'Nur Hasanah, SST', '340050186'),
(136, '340055877', 'Nurhani Restu Umi, SST', '340055877'),
(137, '340017786', 'Nuri Rosmika, SST', '340017786'),
(138, '340012844', 'Nuriah Ismail, SE', '340012844'),
(139, '340011646', 'Nurmalawati, S.E.', '340011646'),
(140, '340013022', 'Nurzakiah, SE', '340013022'),
(141, '340018213', 'Nyimas Eka Yunia Hasnita, S.P', '340018213'),
(142, '340055879', 'Opan Fauzan Hamdan, SST, MEKK', '340055879'),
(143, '340015121', 'Oriza Santifa, S.Si, M.Si.', '340015121'),
(144, '340018969', 'Rais Fahmi, S.P', '340018969'),
(145, '340018216', 'Raiski Ramadhoni, S.E.', '340018216'),
(146, '340054458', 'Rediansyah Arief, SP, M.Si', '340054458'),
(147, '340019957', 'Reza Rezki Pulungan, SST', '340019957'),
(148, '340018566', 'Ridha Mutia, SE, M.Si', '340018566'),
(149, '340057201', 'Rizki Hadiman, SST', '340057201'),
(150, '340010990', 'Sakdani, SE.', '340010990'),
(151, '340015336', 'Tasdik Ilhamudin, S.Si, M.Si', '340015336'),
(152, '340051351', 'Teti Safrida Purba, SE.', '340051351'),
(153, '340016731', 'Titiek Zurriyati, S.Si', '340016731'),
(154, '340054471', 'Wahdini, S.Psi.', '340054471'),
(155, '340016943', 'Wahyu Agung Sutikno, SST', '340016943'),
(156, '340053247', 'Zelvia Yunalda, S.ST', '340053247'),
(157, '340054478', 'Zukhra, SE', '340054478'),
(158, '340014049', 'Zulkarnain', '340014049'),
(159, '340018218', 'Zulviyana', '340018218'),
(160, '340058260', 'FITRI MULYANI, SST', '340058260'),
(161, '340058287', 'ICHSAN HASANUDIN, SST', '340058287'),
(162, '340012245', 'RUBAMA', '340012245');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sigra_master_pegawai`
--
ALTER TABLE `sigra_master_pegawai`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sigra_master_pegawai`
--
ALTER TABLE `sigra_master_pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
